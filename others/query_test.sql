--question a
select c.id, c.name, count(o.id) total_order from customers c
left join orders o on o.customer_id = c.id group by c.id 

--question b
select p.id, p."name", p.code , count(o.id) total_order from products p 
left join order_details od on od.product_id = p.id
left join orders o on o.id = od.order_id
group by p.id order by total_order desc

--question c
select pm.id, pm.name, pm.code, count(o.id) used from payment_methods pm 
left join orders o on o.payment_method_id = pm.id
group by pm.id order by used desc