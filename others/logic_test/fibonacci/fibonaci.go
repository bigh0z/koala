package main

import (
	"fmt"
)

func Fibonacci(indexes int) []int {
	temp := []int{}
	temp = append(temp, 0)
	temp = append(temp, 1)
	for i := 2; i <= indexes-1; i++ {
		temp = append(temp, temp[i-1]+temp[i-2])
	}
	return temp
}

func main() {
	fmt.Printf("%v", Fibonacci(1000))
}
