package main

import (
	"fmt"
)

func isPalindrome(input string) bool {
	reverse := ""
	for _, v := range input {
		reverse = string(v) + reverse
	}

	if input == reverse {
		return true
	}
	return false
}

func main() {
	input := "sugus"
	fmt.Printf("input: %v, isPalindrome: %v", input, isPalindrome(input))
}
