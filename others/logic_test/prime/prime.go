package main

import (
	"fmt"
	"math"
)

func GeneratePrimeSequence(indexes int) []int {
	temp := []int{}
	i := 1
	for {
		if len(temp) == indexes {
			break
		}
		if isPrime(i) {
			temp = append(temp, i)
		}
		i++
	}
	return temp
}

func isPrime(value int) bool {
	for i := 2; i <= int(math.Floor(float64(value)/2)); i++ {
		if value%i == 0 {
			return false
		}
	}
	return value > 1
}

func main() {
	fmt.Printf("%v", GeneratePrimeSequence(10))
}
