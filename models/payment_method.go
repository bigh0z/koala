package models

import (
	"time"
)

type PaymentMethod struct {
	ID        uint64    `gorm:"primaryKey" json:"id"`
	Name      string    `gorm:"not null" json:"name"`
	Code      string    `gorm:"not null" json:"code"`
	CreatedAt time.Time `gorm:"not null" json:"created_at"`
}
