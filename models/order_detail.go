package models

import (
	"time"
)

type OrderDetail struct {
	ID        uint64    `gorm:"primaryKey" json:"id"`
	OrderID   uint64    `gorm:"not null" json:"customer_id"`
	ProductID uint64    `gorm:"not null" json:"product_id"`
	Qty       uint64    `gorm:"not null" json:"qty"`
	CreatedAt time.Time `gorm:"not null" json:"created_at"`
}
