package models

import (
	"time"
)

type Product struct {
	ID         uint64    `gorm:"primaryKey"`
	Name       string    `gorm:"not null" json:"name"`
	BasicPrice uint64    `gorm:"not null" json:"basic_price"`
	CreatedAt  time.Time `gorm:"not null" json:"created_at"`
}
