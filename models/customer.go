package models

import (
	"time"
)

type Customer struct {
	ID          uint64    `gorm:"primaryKey" json:"id"`
	Name        string    `gorm:"not null" json:"name"`
	Gender      string    `gorm:"not null" json:"gender"`
	Password    string    `gorm:"not null" json:"password"`
	Email       string    `gorm:"unique;not null" json:"email"`
	PhoneNumber string    `gorm:"unique;not null" json:"phone_number"`
	BirthDate   time.Time `gorm:"not null" json:"birth_date"`
	CreatedAt   time.Time `gorm:"not null" json:"created_at"`
}
