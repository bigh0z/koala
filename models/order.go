package models

import (
	"koala/utils"
	"time"
)

type Order struct {
	ID              uint64        `gorm:"primaryKey" json:"id"`
	CustomerID      uint64        `gorm:"not null" json:"customer_id"`
	PaymentMethodID uint64        `gorm:"not null" json:"payment_method_id"`
	OrderNumber     string        `gorm:"not null" json:"order_number"`
	OrderDate       time.Time     `gorm:"not null" json:"order_date"`
	CreatedAt       time.Time     `gorm:"not null" json:"created_at"`
	OrderDetails    []OrderDetail `json:"order_details"`
}

func (o *Order) LastNumber() (int64, error) {
	var count int64
	db := utils.ConnDB
	year, month, _ := time.Now().Date()
	query := "EXTRACT(MONTH FROM order_date) = ? AND EXTRACT(YEAR FROM order_date) = ?"
	db.Model(&Order{}).Where(query, int(month), year).Count(&count)
	return count, nil
}
