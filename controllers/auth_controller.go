package controllers

import (
	"fmt"
	"koala/models"
	"koala/services"
	"net/http"

	"github.com/gin-gonic/gin"
)

type AuthController struct{}

type LoginOutput services.LoginOutput

type LoginInput struct {
	Username string `json:"phone_or_email"`
	Password string `json:"password"`
}

func (b *AuthController) Login(c *gin.Context) {
	var services services.AuthService
	var credential LoginInput

	c.BindJSON(&credential)

	result := services.Login(credential.Username, credential.Password)
	if result.Token == "" {
		c.JSON(http.StatusOK, gin.H{"message": "not authorized"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"data": result})
}

func (b *AuthController) Register(c *gin.Context) {
	var services services.AuthService
	var user models.Customer

	c.ShouldBindJSON(&user)

	result, err := services.Register(user)
	if err != nil {
		message := fmt.Sprintf("%v", err)
		c.JSON(http.StatusOK, gin.H{"error": message})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": result})
}

func Auth(c *gin.Context) {
	var services services.AuthService
	tokenString := c.Request.Header.Get("Authorization")
	authorize := services.Authorize(tokenString)
	if !authorize {
		result := gin.H{
			"message": "not authorized",
		}
		c.JSON(http.StatusUnauthorized, result)
		c.Abort()
	}
}
