package controllers

import (
	"fmt"
	"koala/models"
	"koala/services"
	"net/http"

	"github.com/gin-gonic/gin"
)

type OrderController struct{}

func (o *OrderController) Create(c *gin.Context) {
	var services services.OrderService
	var order models.Order
	c.ShouldBindJSON(&order)
	result, err := services.Create(order)
	if err != nil {
		message := fmt.Sprintf("%v", err)
		c.JSON(http.StatusOK, gin.H{"error": message})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": result})
}
