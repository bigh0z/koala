package main

import (
	"fmt"
	"koala/controllers"
	"koala/models"
	"koala/utils"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type Customer models.Customer
type Order models.Order
type OrderDetail models.OrderDetail
type PaymentMethod models.PaymentMethod
type Product models.PaymentMethod

func main() {
	db := utils.DB()
	db.AutoMigrate(
		&Customer{},
		&Order{},
		&OrderDetail{},
		&PaymentMethod{},
		&Product{},
	)

	//seeding
	injectSeed(db)

	var authController controllers.AuthController
	var orderController controllers.OrderController
	router := gin.Default()

	auth := router.Group("api/auth")
	auth.POST("/login", authController.Login)
	auth.POST("/register", authController.Register)

	order := router.Group("api/order")
	order.POST("/", controllers.Auth, orderController.Create)
	fmt.Println("Running server")
	fmt.Println("==========================================")
	router.Run()
}

func injectSeed(db *gorm.DB) {
	password := utils.SetPassword("password")
	db.FirstOrCreate(&Customer{},
		Customer{
			Name:        "bilal",
			Gender:      "Male",
			Password:    password,
			Email:       "bilal@happy5.co",
			PhoneNumber: "085881443053",
			BirthDate:   time.Date(1995, 04, 30, 0, 0, 0, 0, time.UTC),
		},
	)

	db.FirstOrCreate(&Product{},
		Product{
			Name: "Laptop",
			Code: "001",
		},
	)

	db.FirstOrCreate(&Product{},
		Product{
			Name: "Handphone",
			Code: "002",
		},
	)

	db.FirstOrCreate(&PaymentMethod{},
		PaymentMethod{
			Name: "Cash",
			Code: "001",
		},
	)

	db.FirstOrCreate(&PaymentMethod{},
		PaymentMethod{
			Name: "Ovo",
			Code: "002",
		},
	)
}
