package configs

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

const (
	dbhost = "DB_HOST"
	dbport = "DB_PORT"
	dbuser = "DB_USERNAME"
	dbpass = "DB_PASSWORD"
	dbname = "DB_NAME"
)

func Config() map[string]string {
	conf := make(map[string]string)
	host := os.Getenv(dbhost)
	if host == "" {
		panic("DB_HOST environment variable required but not set")
	}
	port := os.Getenv(dbport)
	if port == "" {
		panic("DB_PORT environment variable required but not set")
	}
	user := os.Getenv(dbuser)
	if user == "" {
		panic("DB_USERNAME environment variable required but not set")
	}
	password := os.Getenv(dbpass)
	if password == "" {
		panic("DB_PASSWORD environment variable required but not set")
	}
	name := os.Getenv(dbname)
	if name == "" {
		panic("DB_NAME environment variable required but not set")
	}
	conf[dbhost] = host
	conf[dbport] = port
	conf[dbuser] = user
	conf[dbpass] = password
	conf[dbname] = name
	return conf
}

func init() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalf("Error loading .env file")
	}
}
