package utils

import (
	"strings"
)

func ConvertToRoman(number int) string {
	dictionaries := []struct {
		value int
		digit string
	}{
		{1000, "M"},
		{900, "CM"},
		{500, "D"},
		{400, "CD"},
		{100, "C"},
		{90, "XC"},
		{50, "L"},
		{40, "XL"},
		{10, "X"},
		{9, "IX"},
		{5, "V"},
		{4, "IV"},
		{1, "I"},
	}

	var roman strings.Builder
	for _, dictionary := range dictionaries {
		for number >= dictionary.value {
			roman.WriteString(dictionary.digit)
			number -= dictionary.value
		}
	}
	return roman.String()
}
