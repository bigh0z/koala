package utils

import (
	"fmt"
	"koala/configs"
	"log"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var ConnDB *gorm.DB

func DB() (db *gorm.DB) {
	config := configs.Config()
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		config["DB_HOST"], config["DB_PORT"],
		config["DB_USERNAME"], config["DB_PASSWORD"], config["DB_NAME"])

	connect := func() *gorm.DB {
		db, err := gorm.Open(postgres.Open(psqlInfo), &gorm.Config{})

		if err != nil {
			log.Println(err)
			panic("Database Connection Failed!")
		} else {
			log.Println("Database Connection Establish!")
		}

		return db
	}

	if ConnDB == nil {
		ConnDB = connect()
	}

	return ConnDB
}
