package utils

import (
	"log"

	"golang.org/x/crypto/bcrypt"
)

func SetPassword(password string) string {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		log.Fatal(err)
	}
	return string(hash)
}

func ComparePassword(hashPassword string, plain string) bool {
	hash := []byte(hashPassword)
	if err := bcrypt.CompareHashAndPassword(hash, []byte(plain)); err != nil {
		return false
	}
	return true
}
