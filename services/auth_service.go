package services

import (
	"fmt"
	"koala/configs"
	"koala/models"
	"koala/utils"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

type AuthService struct{}

type LoginOutput struct {
	Token string          `json:"token"`
	User  models.Customer `json:"user"`
}

var config = configs.Config()

func (o *AuthService) Register(user models.Customer) (models.Customer, error) {
	password := user.Password
	user.Password = utils.SetPassword(password)

	tx := utils.ConnDB.Begin()

	err := tx.Create(&user).Error
	if err != nil {
		tx.Rollback()
		return models.Customer{}, err
	}
	tx.Commit()
	return user, nil
}

func (o *AuthService) Login(phoneORmail string, password string) LoginOutput {
	var user models.Customer
	var result LoginOutput

	whereClause := "(email = ? OR phone_number = ?)"
	db := utils.ConnDB
	db.Where(whereClause, phoneORmail,
		phoneORmail,
	).First(&user)

	isCorrectPassword := utils.ComparePassword(user.Password, password)
	if user == (models.Customer{}) {
		return result
	} else if !isCorrectPassword {
		return result
	}

	result.Token = generateToken(user)
	result.User = user

	return result
}

func (o *AuthService) Authorize(tokenString string) bool {
	tokenString = extractToken(tokenString)
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if jwt.GetSigningMethod("HS256") != token.Method {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(config["SALT"]), nil
	})

	if token != nil && err == nil {
		claims := token.Claims.(jwt.MapClaims)
		if !expired(claims["exp"]) {
			return true
		}
	}
	return false
}

func generateToken(user models.Customer) string {
	tomorrow := time.Now().Add(24 * time.Hour)

	sign := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"id":    user.ID,
		"email": user.Email,
		"exp":   tomorrow,
	})
	token, _ := sign.SignedString([]byte(config["SALT"]))
	return token
}

func extractToken(bearToken string) string {
	strArr := strings.Split(bearToken, " ")
	if len(strArr) == 2 {
		return strArr[1]
	}
	return ""
}

func expired(exp interface{}) bool {
	layout := "2006-01-02T15:04:05.000Z"
	strDate := fmt.Sprintf("%v", exp)
	tokenDate, _ := time.Parse(layout, strDate)
	today := time.Now()
	if tokenDate.After(today) {
		return true
	}

	return false
}
