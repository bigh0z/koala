package services

import (
	"fmt"
	"koala/models"
	"koala/utils"
	"time"
)

type OrderService struct{}

func (o *OrderService) Create(order models.Order) (models.Order, error) {
	tx := utils.ConnDB.Begin()

	lastNumber, err := order.LastNumber()
	order.OrderDate = time.Now()
	order.OrderNumber = generateOrderNumber(lastNumber + 1)

	err = tx.Create(&order).Error
	if err != nil {
		tx.Rollback()
		return models.Order{}, err
	}
	tx.Commit()
	return order, nil
}

func generateOrderNumber(lastNumber int64) string {
	year, month, _ := time.Now().Date()
	romanNumber := utils.ConvertToRoman(int(month))
	orderNumber := fmt.Sprintf("PO-%v/%v/%v",
		lastNumber,
		romanNumber,
		year,
	)
	return orderNumber
}
